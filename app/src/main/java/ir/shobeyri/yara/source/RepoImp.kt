package ir.shobeyri.yara.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import ir.shobeyri.yara.data.GetMovieDetails
import ir.shobeyri.yara.data.GetMovieListModel
import ir.shobeyri.yara.data.Search
import ir.shobeyri.yara.db.DaoInterface
import ir.shobeyri.yara.source.base.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class RepoImp @Inject constructor(
    var dao: DaoInterface,var api: Api
) : Repo {

    suspend fun getMovieList() : LiveData<List<GetMovieListModel>> = dao.getAllGetMovieListModel().also {
        CoroutineScope(IO).launch {
            val data = req {
                api.getMovieList("batman")
            }
            data.data?.let {
                dao.insertGetMovieListModel(it)
            }
        }
    }

    suspend fun getDetail(id : String) : LiveData<BaseModel<GetMovieDetails>> = liveData{
        val data = req {
            api.getMovieDetails(id)
        }
        emit(data)
    }

    override suspend fun <T> req(req : suspend () -> T) : BaseModel<T> {
        try {
            return BaseModel(req.invoke())
        }catch (e : Exception){
            return BaseModel(null,e) // Network error or sth like that
        }
    }

}