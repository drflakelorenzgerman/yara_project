package ir.shobeyri.yara.source.base

import ir.shobeyri.yara.source.BaseModel

interface Repo {
    suspend fun <T> req(req : suspend () -> T) : BaseModel<T>
}