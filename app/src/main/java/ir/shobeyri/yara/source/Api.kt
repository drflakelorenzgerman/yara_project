package ir.shobeyri.yara.source

import ir.shobeyri.yara.data.GetMovieDetails
import ir.shobeyri.yara.data.GetMovieListModel
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface Api {
    @GET("/")
    suspend fun getMovieList(@Query("s") movieName : String) : GetMovieListModel

    @GET("/")
    suspend fun getMovieDetails(@Query("i") movieName : String) : GetMovieDetails
}