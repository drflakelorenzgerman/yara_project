package ir.shobeyri.yara.source

import ir.shobeyri.yara.utils.Constant.DEFAULT_TAG_GeneralDataState
import java.lang.Exception


sealed class GeneralDataState<out ViewState>{

    data class Response<ViewState>(val data : ViewState?,val tag : String = DEFAULT_TAG_GeneralDataState) : GeneralDataState<ViewState>()
    data class Failed<ViewState>(val data : ViewState?,val error : Exception,val tag : String = DEFAULT_TAG_GeneralDataState) : GeneralDataState<ViewState>()
    data class Loading<ViewState>(val data : ViewState?,val message : String,val tag : String = DEFAULT_TAG_GeneralDataState) : GeneralDataState<ViewState>()
    class Nothing<ViewState>(val tag : String = DEFAULT_TAG_GeneralDataState) : GeneralDataState<ViewState>()

    fun toData() : ViewState? = when(this){
        is  Response-> this.data
        is  Failed-> this.data
        is  Loading-> this.data
        else -> null
    }

    fun type() : String = when(this){
        is  Response-> "Response"
        is  Failed-> "Failed"
        is  Loading-> "Loading"
        else -> "Cleared"
    }

    fun tag() : String = when(this){
        is  Response-> tag
        is  Failed-> tag
        is  Loading-> tag
        is  Nothing -> tag
    }
}

