package ir.shobeyri.yara.source

import java.lang.Exception

data class BaseModel<T>(val data : T?,val e : Exception? = null)