package ir.shobeyri.yara.utils

object Constant {
    const val DEFAULT_TAG_GeneralDataState = "GeneralDataState"
    const val DB_NAME = "DB_NAME"
    const val TABLE_GetMovieListModel = "movie_list_table"
    const val TABLE_GetMovieDetails = "detail_table"
}