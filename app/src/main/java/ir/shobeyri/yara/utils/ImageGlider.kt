package ir.shobeyri.yara.utils

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.configGlide(url : String){
    Glide.with(this).load(url).into(this)
}