package ir.shobeyri.yara.di

import android.content.Context
import androidx.room.Dao
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.shobeyri.yara.db.DaoInterface
import ir.shobeyri.yara.db.Db
import ir.shobeyri.yara.source.Api
import ir.shobeyri.yara.source.RepoImp
import ir.shobeyri.yara.source.base.Repo
import ir.shobeyri.yara.utils.Constant.DB_NAME
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FragmentModule {
    @Provides
    fun provideRetrofit() : Api =
        Retrofit.Builder()
            .baseUrl("http://www.omdbapi.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().addInterceptor(object : Interceptor{
                override fun intercept(chain: Interceptor.Chain): Response {
                    return chain.proceed(chain.request().newBuilder()
                        .url(chain.request().url().newBuilder().addQueryParameter("apiKey","3e974fca").build())
                        .build())
                }

            }).build())
            .build()
            .create(Api::class.java)


    @Singleton
    @Provides
    fun provideDb(@ApplicationContext context: Context) : Db = Room.databaseBuilder(context,Db::class.java,DB_NAME).build()



    @Singleton
    @Provides
    fun provideDao(db : Db) : DaoInterface = db.dao()

//    @Provides
//    fun provideRepo(dao : DaoInterface,api: Api) : Repo = RepoImp(api,)
}