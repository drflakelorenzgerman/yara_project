package ir.shobeyri.yara.ui.fragment.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.shobeyri.yara.data.GetMovieDetails
import ir.shobeyri.yara.source.BaseModel
import ir.shobeyri.yara.source.GeneralDataState
import ir.shobeyri.yara.source.RepoImp
import ir.shobeyri.yara.ui.base.BaseViewModel
import ir.shobeyri.yara.ui.base.SingleLiveEvent
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class BatmanListFragmentViewModel @Inject constructor(var repo : RepoImp)
    : BaseViewModel<BatmanListFragmentState, BatmanListFragmentIntent>() {

    private val _viewEffects: SingleLiveEvent<BatmanListFragmentViewEffect> = SingleLiveEvent()
    val viewEffects: LiveData<BatmanListFragmentViewEffect> = _viewEffects


    override fun action(intent: BatmanListFragmentIntent) {
        when(intent){
            is BatmanListFragmentIntent.getMovies->{
                getMovies()
            }
            is BatmanListFragmentIntent.clickOnMovie->{
                getDetail(intent.id)
            }
            is BatmanListFragmentIntent.navigate->{
                clearDataState()
                _viewEffects.value = BatmanListFragmentViewEffect.navigateToDetail(intent.data)
            }
        }
    }

    private fun getMovies(){
        runRequest {
            Transformations.map(repo.getMovieList()){
                if(it.isEmpty()){
                    _dataStates.postValue(GeneralDataState.Failed<BatmanListFragmentState>(
                        BatmanListFragmentState(), Exception("No Data")
                    ))
                }else{
                    val firstRow = it.first()
                    _dataStates.postValue(GeneralDataState.Response<BatmanListFragmentState>(
                        BatmanListFragmentState(getMovieListModel = firstRow)
                    ))
                }
            }.observeForever {

            }
        }
    }

    private fun getDetail(id : String){
        runRequest {
            Transformations.map(repo.getDetail(id)){
                checkValueNotNull(it)?.let {
                    _dataStates.postValue(GeneralDataState.Response<BatmanListFragmentState>(
                        BatmanListFragmentState(getMovieDetails = it)
                    ))
                }
            }.observeForever {

            }
        }
    }

}