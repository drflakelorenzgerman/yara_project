package ir.shobeyri.yara.ui.base


import androidx.lifecycle.*
import ir.shobeyri.yara.source.BaseModel
import ir.shobeyri.yara.source.GeneralDataState
import ir.shobeyri.yara.ui.fragment.list.BatmanListFragmentState
import ir.shobeyri.yara.utils.Constant.DEFAULT_TAG_GeneralDataState
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main


abstract class BaseViewModel<T,Intent> : ViewModel() {

    protected val _dataStates: MutableLiveData<GeneralDataState<T>> = MutableLiveData()
    val dataStates: LiveData<GeneralDataState<T>> = _dataStates

    protected fun getViewState() : T? = _dataStates.value?.toData()

    abstract fun action(intent : Intent)

    fun clearDataState(){
        _dataStates.value = GeneralDataState.Nothing()
    }

    fun runRequest(nameTag : String = DEFAULT_TAG_GeneralDataState,req : suspend() -> Unit) {
        CoroutineScope(Main).launch {
            val currentState = getViewState()
            try {
                _dataStates.postValue(GeneralDataState.Loading(currentState,"Loading...",nameTag))
                req()
            }catch (exception: Exception) {
                _dataStates.postValue(GeneralDataState.Failed(currentState, exception,nameTag))
            }
        }
    }


    fun <Type> checkValueNotNull(model: BaseModel<Type>): Type?{
        if(model.data != null){
            return model.data
        }else if(model.e != null){
            val currentState = getViewState()
            _dataStates.postValue(GeneralDataState.Failed(currentState, model.e))
        }else{
            val currentState = getViewState()
            _dataStates.postValue(GeneralDataState.Failed(currentState,Exception("ERROR")))
        }
        return null
    }


}
