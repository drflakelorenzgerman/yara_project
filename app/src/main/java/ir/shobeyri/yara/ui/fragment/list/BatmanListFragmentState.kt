package ir.shobeyri.yara.ui.fragment.list

import ir.shobeyri.yara.data.GetMovieDetails
import ir.shobeyri.yara.data.GetMovieListModel

data class BatmanListFragmentState(
    val getMovieListModel : GetMovieListModel? = null,
    val getMovieDetails : GetMovieDetails? = null
)