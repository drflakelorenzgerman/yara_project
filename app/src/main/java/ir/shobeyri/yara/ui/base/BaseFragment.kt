package ir.shobeyri.yara.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import ir.shobeyri.yara.ui.activity.MainActivity

abstract class  BaseFragment<DB : ViewDataBinding> : Fragment(){

    abstract val layout : Int
    lateinit var dataBinding : DB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater,layout,container,false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding()
    }

    fun loading(on : Boolean = true){
        (requireActivity() as MainActivity).loading(on)
    }

    abstract fun binding()
}
