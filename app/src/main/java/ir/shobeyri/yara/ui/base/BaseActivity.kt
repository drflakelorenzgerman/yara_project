package ir.shobeyri.yara.ui.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity() {
    abstract val layout : Int
    lateinit var dataBinding : DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(this),layout,null,false)
        setContentView(dataBinding.root)
        binding()
    }

    abstract fun binding()
    abstract fun loading(on: Boolean)
}