package ir.shobeyri.yara.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.yara.R
import ir.shobeyri.yara.databinding.ActivityMainBinding
import ir.shobeyri.yara.ui.base.BaseActivity

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val layout: Int
        get() = R.layout.activity_main

    override fun binding() {
    }

    override fun loading(on: Boolean) {
        if(on)
            dataBinding.progressCircular.visibility = View.VISIBLE
        else
            dataBinding.progressCircular.visibility = View.GONE
    }

}