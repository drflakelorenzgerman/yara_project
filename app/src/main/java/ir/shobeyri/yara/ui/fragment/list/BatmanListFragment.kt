package ir.shobeyri.yara.ui.fragment.list

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.yara.R
import ir.shobeyri.yara.data.Search
import ir.shobeyri.yara.databinding.FragmentBatmanListBinding
import ir.shobeyri.yara.databinding.ItemBatmanListBinding
import ir.shobeyri.yara.source.GeneralDataState
import ir.shobeyri.yara.ui.base.BaseAdapter
import ir.shobeyri.yara.ui.base.BaseFragment
import ir.shobeyri.yara.utils.configGlide

@AndroidEntryPoint
class BatmanListFragment : BaseFragment<FragmentBatmanListBinding>() {
    override val layout: Int
        = R.layout.fragment_batman_list

    val viewModel by viewModels<BatmanListFragmentViewModel>()

    override fun binding() {

        viewModel.viewEffects.observe(viewLifecycleOwner, Observer {
            when(it){
                is BatmanListFragmentViewEffect.navigateToDetail->{
                    findNavController().navigate(BatmanListFragmentDirections.actionBatmanListFragmentToDetailFragment(
                        it.data
                    ))
                }
            }
        })

        viewModel.dataStates.observe(viewLifecycleOwner, Observer {
            when(it){
                is GeneralDataState.Failed->{
                    loading(false)
                    Toast.makeText(requireContext(),"ERROR",Toast.LENGTH_SHORT).show()
                }
                is GeneralDataState.Response->{
                    loading(false)
                    it.toData()?.let {
                        it.getMovieListModel?.let {
                            configList(it.Search)
                        }
                        it.getMovieDetails?.let {
                            viewModel.action(BatmanListFragmentIntent.navigate(it))
                        }
                    }
                }
                is GeneralDataState.Loading->{
                    loading()
                }
            }
        })

        viewModel.action(BatmanListFragmentIntent.getMovies)
    }

    fun configList(list : List<Search>){
        val adapter = object : BaseAdapter<ItemBatmanListBinding,
                Search>(list.toMutableList(),R.layout.item_batman_list){
            override fun bind(binding: ItemBatmanListBinding, item: Search, position: Int) {
                binding.apply {
                    txtTitle.text = "Title : "+item.Title
                    txtType.text = "Type : "+item.Type
                    txtYear.text = "Year : "+item.Year
                    img.configGlide(item.Poster)
                }

                binding.btn.setOnClickListener {
                    viewModel.action(BatmanListFragmentIntent.clickOnMovie(item.imdbID))
                }
            }
        }
        dataBinding.list.adapter = adapter
        dataBinding.list.layoutManager = LinearLayoutManager(requireContext())
    }
}