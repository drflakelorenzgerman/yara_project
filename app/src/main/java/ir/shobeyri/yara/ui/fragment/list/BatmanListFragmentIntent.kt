package ir.shobeyri.yara.ui.fragment.list

import ir.shobeyri.yara.data.GetMovieDetails

sealed class BatmanListFragmentIntent {
    object getMovies : BatmanListFragmentIntent()
    class clickOnMovie(val id : String) : BatmanListFragmentIntent()
    class navigate(val data : GetMovieDetails) : BatmanListFragmentIntent()
}