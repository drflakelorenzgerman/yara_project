package ir.shobeyri.yara.ui.fragment.detail

import androidx.navigation.fragment.navArgs
import ir.shobeyri.yara.R
import ir.shobeyri.yara.databinding.FragmentDetailBinding
import ir.shobeyri.yara.ui.base.BaseFragment
import ir.shobeyri.yara.utils.configGlide

class DetailFragment : BaseFragment<FragmentDetailBinding>() {
    override val layout: Int
        get() = R.layout.fragment_detail

    val data by navArgs<DetailFragmentArgs>()

    override fun binding() {
        dataBinding.data = data.data

        dataBinding.img.configGlide(data.data.Poster)
    }
}