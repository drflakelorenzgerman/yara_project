package ir.shobeyri.yara.ui.fragment.list

import ir.shobeyri.yara.data.GetMovieDetails

sealed class BatmanListFragmentViewEffect {
    class navigateToDetail(var data : GetMovieDetails) : BatmanListFragmentViewEffect()
}