package ir.shobeyri.yara.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ir.shobeyri.yara.data.GetMovieDetails
import ir.shobeyri.yara.data.GetMovieListModel
import ir.shobeyri.yara.data.Search
import ir.shobeyri.yara.utils.Constant.DB_NAME
import ir.shobeyri.yara.utils.Constant.TABLE_GetMovieDetails
import ir.shobeyri.yara.utils.Constant.TABLE_GetMovieListModel

@Dao
interface  DaoInterface {
    @Insert
    fun insertGetMovieListModel(movie_list : GetMovieListModel)

    @Query("DELETE FROM ${TABLE_GetMovieListModel}")
    fun deleteGetMovieListModel()


    @Query("SELECT * FROM ${TABLE_GetMovieListModel}")
    fun getAllGetMovieListModel() : LiveData<List<GetMovieListModel>>

}