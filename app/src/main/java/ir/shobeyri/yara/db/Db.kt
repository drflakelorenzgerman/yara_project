package ir.shobeyri.yara.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ir.shobeyri.yara.data.GetMovieListModel
import ir.shobeyri.yara.data.SearchConverters

@Database(entities = [GetMovieListModel::class],version = 1)
@TypeConverters(SearchConverters::class)
abstract class Db : RoomDatabase(){
    abstract fun dao() : DaoInterface
}