package ir.shobeyri.yara.data

import androidx.room.*
import com.google.gson.Gson
import ir.shobeyri.yara.utils.Constant

@Entity(tableName = Constant.TABLE_GetMovieListModel)
data class GetMovieListModel(
    @PrimaryKey
    val id : Int?,
    val Response: String,
    @TypeConverters(SearchConverters::class)
    val Search: List<Search>,
    val totalResults: String
)

data class Search(
    val Poster: String,
    val Title: String,
    val Type: String,
    val Year: String,
    val imdbID: String
)

class SearchConverters {
    companion object{
        @TypeConverter
        @JvmStatic
        fun stringToSearch(value: String): List<Search>
                = Gson().fromJson<Array<Search>>(value,Array<Search>::class.java).toList()

        @JvmStatic
        @TypeConverter
        fun searchToString(list: List<Search>): String = Gson().toJson(list.toTypedArray())
    }
}

